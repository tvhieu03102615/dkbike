"use strict";
jQuery(document).ready(function($) {
	// Window on load
	$(window).on('load', function() {
			// Khanh js
			// ------- Menu -------
			var layscroll;
			$(".menu-res").click(function(event) {
				$(".menu ul").toggleClass('active');;
				layscroll = $(window).scrollTop();
			});
			$(window).scroll(function(event) {
				var scroolSau = $(window).scrollTop();
					if (scroolSau != layscroll) {
							$(".menu ul").removeClass('active');
				}
			});
			// $(window).resize(function(event) {
			// 	if ($(window).width() > 768) {
			// 			$(".menu ul").slideDown(0);
			// 	} else {
			// 			$(".menu ul").slideUp(0);
			// 	}
			// });
			//  ------- slide home ------
			$('.slide__home .owl-carousel').owlCarousel({
					loop: true,
					margin: 0,
					nav: true,
					items: 1,
			});
			// validate - lien he
			$("#form-lienhe").validate({
					onfocusout: false,
					onkeyup: false,
					onclick: false,
					rules: {
							"hoten": {
									required: true,
									minlength: 6,
									maxlength: 33,
							},
							"diachiemail":{
									required: true,
									email: true,
									maxlength: 100,
							},
							"sdthoai": {
									required: true,
									minlength: 10,
									maxlength: 11,
									number: true,
							}
					},
					messages: {
						"hoten": {
								required: "* Bắt buộc nhập họ và tên.",
								maxlength: jQuery.validator.format("* Lớn nhất có 33 kí tự"),
								minlength: jQuery.validator.format("* Ít nhất phải có 6 kí tự"),
						},
						"diachiemail": {
								required: "* Bắt buộc nhập email.",
								email: "* Email của bạn sai định dạng.",
						},
						"sdthoai": {
								required: "* Bắt buộc nhập số điện thoại.",
								maxlength: jQuery.validator.format("* Đã vượt quá 11 chữ số."),
								minlength: jQuery.validator.format("* Ít nhất phải có 10 chữ số."),
								number: "* Phải là số điện thoại.",
						}
					}
			});
			// Tuan js
			$(window).resize(function() {
					if ($(window).width() > 720) {
							$(".products__filter__inner").css("display", "block");
					} else {
							$(".products__filter__inner").css("display", "none");
					}
			});
			// js for products_left
			$('.products__filter__inner ul li').click(function(e) {
					e.preventDefault();
					$(this).siblings().find('i').removeClass('check-active');
					$(this).find('i').addClass('check-active');
			});
			//js for product-detail
			$('.p-detail__row--col2 li').click(function(e) {
					e.preventDefault();
					$(this).siblings().removeClass('setactive');
					$(this).addClass('setactive');
					var stringParent = $(this).parent().parent().parent().attr('id');
					var index = $(this).index() + 1;
					var stringIndex = '#' + stringParent + ' .p-detail__row--col1__item:nth-child(' + index + ')';
					$(stringIndex).siblings().removeClass('setactive-main');
					$(stringIndex).addClass('setactive-main');
			});
			//js for product-detail. Choose color for vehicle
			$('.specifications__color a').click(function (e) { 
				e.preventDefault();
				var urlimg = $(this).attr('img-src');
				var altimg = urlimg.substring(7);
				console.log(altimg);
				$('.specifications__img img').attr({
					'src':urlimg,
					'alt':altimg
				});
			});
			//js for specifications
			$('.specifications__inner').mCustomScrollbar({
					theme: "customByTuan",
					scrollInertia: 300,
					mouseWheelPixels: 200
			});
			//js for filter
			$('.products__filter__icon').click(function(e) {
					e.preventDefault();
					$('.products__filter__inner').slideToggle(250);
			});
			// $('.products__filter__inner').slideUp();
			// Chuc js
			$('.depart').owlCarousel({
					loop: true,
					margin: 10,
					nav: true,
					autoplay: true,
					autoplayTimeout: 2000,
					autoplayHoverPause: true,
					responsive: {
							0: {
									items: 1
							},
							600: {
									items: 1
							},
							1000: {
									items: 1
							}
					}
			})
			$("#select").chosen();
			$(".select__top .chosen-container-single").css("width", "100%")
	});
});

function openNav() {
		document.getElementById("toggle").classList.toggle("show");
}

function closeNav() {
		document.getElementById("toggle").classList.toggle("show");
}